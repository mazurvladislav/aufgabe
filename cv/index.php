<!DOCTYPE html>
<html>
<head>
    <title>Test</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="js/jquery-3.4.1.min.js"></script>

    <script src="js/uikit-icons.min.js"></script>
    <script src="js/uikit.min.js"></script>
    <script src="js/custom.js"></script>

    <link rel="stylesheet" href="css/uikit.min.css"/>


    <link rel="stylesheet" href="css/body.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
    <link rel="stylesheet" href="css/stylesheet.css"/>


</head>

<body>


<div class="uk-container">
    <div class="uk-section">
        <div class="" uk-grid>
            <div class="uk-width-1-2@m">
                <h1>
                    Test yourself for the coronavirus and learn howto protect yourself!
                </h1>
                <p>Coughing? Feverish? Worried about the coronavirus? First, do not panic, or it can go sideways really
                    fast
                </p>
                <p>The World Health Organization (WHO) has declared a global health emergency over a coronavirus
                    situation calling it the pandemic spreading all across the globe. British scientists conducted a
                    study on the coronavirus suggesting the Death Rate of the coronavirus rising to 27% during the next
                    month.</p>
                <p>So, the prognosis is not good, and we must protect ourselves and our families from the disease. And
                    now it’s surely not the rightest time to let slide and relax!</p>
                <div class="btn-cont">
                    <a class="uk-button  btn-pass" href="#modal-sections" uk-toggle>Pass the test</a>
                </div>
            </div>
            <div class="uk-width-1-2@m">
                <div class="bg-sect-1">
                    <img src="img/b-1.png" alt="">
                    <img class="warn" src="img/warn.png">
                </div>
            </div>
        </div>
    </div>


    <div id="modal-sections" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h2 class="uk-modal-title"></h2>
            </div>


            <div class="uk-alert-danger alert-error" uk-alert style="display: none">
                <a class="uk-alert-close" uk-close></a>
                <p>Pls feel all</p>
            </div>

<div class="uk-container">
            <form id="regForm" action="/new">
                <div class="uk-grid-medium" uk-grid>
                    <div class="uk-width-1-2@m ">
                        <h2 class="">Here are the questions you should ask yourself to assess your risk.</h2>
                        <!-- Circles which indicates the steps of the form: -->
                        <h2 class="ask">Ask:</h2>
                        <div>
                            <div class="steps">
                                <span class="step">1</span>
                                <span class="step"> - 2</span>
                                <span class="step"> - 3</span>
                                <span class="step"> - 4</span>
                                <span class="step"> - 5</span>
                                <span class="step"> - 6</span>
                                <span class="step"> - 7</span>
                                <span class="step"> - 8</span>
                                <span class="step"> - 9</span>
                                <span class="step"> - 10</span>
                                <span class="step" style="display: none"> - 11</span>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-2@m uk-visible@m"  >
                        <h2 class="ask center des">
                            Disease risk level
                        </h2>
                        <h3 class="perp center "></h3>
                        <h6 class="health"> Your health is at risk!</h6>
                    </div>
                </div>


                <!-- One "tab" for each step in the form: -->
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you met sick, sneezing or coughing people recently? </p>
                                <label><input class="uk-radio" type="radio" name="yesno1" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno1" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph1.png" alt="" class="img">

                        </div>
                    </div>

                </div>

                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno2" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno2" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-2.png" alt="" class="img">

                        </div>
                    </div>

                </div>

                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno3" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno3" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-3.png" alt="" class="img">

                        </div>
                    </div>

                </div>

                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno4" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno4" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-4.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno5" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno5" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-6.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno6" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno6" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-7.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno7" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno7" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-8.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno8" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno8" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-9.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno9" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno9" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-10.png" alt="" class="img">

                        </div>
                    </div>

                </div>
                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin ">
                                <p>Have you felt sick, tired or exhausted recently?</p>
                                <label><input class="uk-radio" type="radio" name="yesno10" value="1" checked> Yes</label>
                                <label><input class="uk-radio" type="radio" name="yesno10" value="2"> No</label>
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <img src="img/ph-11.png" alt="" class="img">

                        </div>
                    </div>

                </div>

                <div class="tab">
                    <div class="uk-grid-medium" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-margin">
                                <p><b>Richard Milner, Head of the SCRC</b></p>
                                <p>If you are thinking about whether you are infected with the virus, you need to take action immediately. Because your and your family members’ lives and health are only in your hands. You might not even know that you have it! But the people the most important for you, can get sick and suffer from the virus.</p>
                                <p>You can get the coronavirus even when attending a small house party where no one will be coughing, sneezing or otherwise displaying any symptoms of illness! You can get sick only because of the short trip to a hardware store nearby.</p>
                                <p>All our work here, at SCRC, intended to save as many people as possible, by giving them all up-to-date facts about the coronavirus. So we created the COMPLETE MANUAL that will tell you everything you need to know about the coronavirus infection. And you will stay healthy and protected!</p>
                                <input class="uk-input inputemail" type="email" name="email" placeholder="Your Email for last News">
                            </div>
                        </div>
                        <div class="uk-width-1-2@m center">
                            <div class="bg-sect-1 marg-50">
                                <img src="img/result.png" alt="" class="img result">
                                <img class="warn" src="img/warn.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-grid-medium" uk-grid>
                    <div class="uk-width-1-2@m">
                        <div style="overflow:auto;">
                            <div class="">
                                <button class="uk-button uk-button-default btn-pass-test click" type="button" id="prevBtn"
                                        onclick="nextPrev(-1)">
                                    Back
                                </button>
                                <button class="uk-button uk-button-default btn-pass-test click" type="button" id="nextBtn"
                                        onclick="nextPrev(1)">
                                    Next
                                </button>
                                <input id="submbut" class="uk-button uk-button-default result-end click" type="submit" value="Download recommendations" style="display: none">

                            </div>
                            <p>It’s highly recommended for you to check out the newest
                                recommendations of the Special Coronavirus Research
                                Center about the virus.
                            </p>
                        </div>

                    </div>
                    <div class="uk-width-1-2@m"></div>
                </div>


            </form>
        </div>
        </div>

    </div>
</div>


<script>
    var currentTab = 0; // Current tab is set to be the first tab (0)
    showTab(currentTab); // Display the current tab

    function showTab(n) {
        // This function will display the specified tab of the form...
        var x = document.getElementsByClassName("tab");
        x[n].style.display = "block";
        x[n].addClass = "act";
        //... and fix the Previous/Next buttons:
        if (n == 0) {
            document.getElementById("prevBtn").style.display = "none";
        } else {
            document.getElementById("prevBtn").style.display = "inline";
        }
        if (n == (x.length - 1)) {
            document.getElementById("nextBtn").innerHTML = "Submit";
        } else {
            document.getElementById("nextBtn").innerHTML = "Next";
        }
        //... and run a function that will display the correct step indicator:
        fixStepIndicator(n)
    }

    function nextPrev(n) {
        // This function will figure out which tab to display
        var x = document.getElementsByClassName("tab");
        // Exit the function if any field in the current tab is invalid:
        if (n == 1 && !validateForm()) return false;
        // Hide the current tab:
        x[currentTab].style.display = "none";
        // Increase or decrease the current tab by 1:
        currentTab = currentTab + n;
        // if you have reached the end of the form...
        if (currentTab >= x.length) {
            // ... the form gets submitted:
            document.getElementById("regForm").submit();
            return false;
        }
        // Otherwise, display the correct tab:
        showTab(currentTab);
    }

    function validateForm() {

        //validate radio
        /*
                var radios = document.getElementsByName("yesno");
                var formValid = false;

                var d = 0;
                while (!formValid && d < radios.length) {
                    if (radios[d].checked) formValid = true;
                    $(".alert-error").hide();
                    d++;

                }

                if (!formValid) {
                    $(".alert-error").show();
                }
                return formValid;*/


        // This function deals with validation of the form fields
        var x, y, i, valid = true;
        x = document.getElementsByClassName("tab");
        y = x[currentTab].getElementsByTagName("input");
        // A loop that checks every input field in the current tab:
        for (i = 0; i < y.length; i++) {
            // If a field is empty...
            if (y[i].value == "") {
                // add an "invalid" class to the field:
                y[i].className += " invalid";
                // and set the current valid status to false
                valid = false;
            }
        }
        // If the valid status is true, mark the step as finished and valid:
        if (valid) {
            document.getElementsByClassName("step")[currentTab].className += " finish";
        }
        return valid; // return the valid status
    }

    function fixStepIndicator(n) {
        // This function removes the "active" class of all steps...
        var i, x = document.getElementsByClassName("step");

        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }

        //... and adds the "active" class on the current step:
        x[n].className += " active";
    }
</script>
</body>
</html>