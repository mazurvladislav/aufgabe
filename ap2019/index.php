<!DOCTYPE html>
<html>
<head>
    <title>Die Mühlenbäcker</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.min.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>
</head>
<body>
<header>
    <div class="bg-menu uk-position-relative">
        <div class="bg-white" uk-sticky="top: 100; animation: uk-animation-slide-top; bottom: #sticky-on-scroll-up">
            <div class="uk-container">
                <nav class="uk-navbar-container uk-navbar-transparent uk-margin" uk-navbar>
                    <div class="uk-navbar-left">
                        <a class="uk-navbar-item uk-logo" href="#">
                            <img src="img/logo-black.png">
                        </a>
                    </div>

                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav">
                            <li><a href="#">Event</a></li>
                            <li><a href="#">Termine</a></li>
                            <li><a href="#">Anmeldung</a></li>
                            <li><a href="#">Rezepte</a></li>
                            <li><a href="#">Kontakt</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="uk-position-relative  uk-visible-toggle uk-light" tabindex="-1" uk-slideshow="ratio: false">
        <ul class="uk-slideshow-items" uk-height-viewport="offset-top: true;">
            <li>
                <img src="img/header.jpg" alt="" uk-cover>
                <div class="uk-position-center uk-position-small uk-text-center uk-light">
                    <p class="uk-margin-remove getreide">GETREIDEMÜHLE KORN UND SCHROT PRÄSENTIERT</p>
                    <h1 class="uk-margin-remove text-slide">Die Mühlenbäcker</h1>

                </div>
            </li>

        </ul>
    </div>
</header>
<div class="inhalt">

    <section id="block1" class="first" style="background-image: url(img/block1.jpg)">
        <div class="uk-container">
            <div uk-grid>

                <div class="uk-width-1-3@m">

                </div>

                <div class="uk-width-2-3@m">
                    <h2>Worauf Sie sich freuen dürfen</h2>
                    <p class="uk-column-1-2@m">Gemeinsam zubereitete schmackhafte Semmeln und Gebäck mit frischen
                        Zutaten aus unserer eigenen
                        Mühle – ökologisch, regional und erschwinglich.
                        Sie erleben Spaß und Genuss beim gemeinsamen Backen und Schlemmen. Lassen Sie sich überraschen,
                        welche/r Bäckermeister/in bei Ihrer Mühlenbäckerei die Anleitung übernimmt!
                        Außerdem eröffnen Sie sich eine große Chance: Alle Teilnehmer der Mühlenbäckerei können unsere
                        Mehlsorten in einen Spezial-Angebot bestellen.
                        Eingeladen zur Mühlenbäckerei sind: junge und junggebliebene ernährungsbewusste Hobbybäcker aus
                        unserer Region Straubing.
                    </p>
                    <div>Unkostenbeitrag: EUR 20,–</div>
                    <div>Anmeldeschluss: 1. Oktober 2019</div>
                </div>
            </div>
        </div>
    </section>
    <section id="block2">
        <div class="uk-container">
            <div uk-grid>

                <div class="uk-width-1-2@m">
                    <img src="img/bg-people.png">
                </div>
                <div class="uk-width-1-2@m">
                    <h2>4 × in diesem Herbst</h2>
                    <ul class="event-list">
                        <li>jeweils ab 14.30 Uhr</li>
                        <li>in unserer hauseigenen Mühlenbäckerei</li>
                        <li>bei der Getreidemühle Korn und Schrot, Schanzlweg 37, 94315 Straubing</li>
                    </ul>

                </div>
            </div>
        </div>
    </section>
    <section id="block3" class="bg-form">
        <div class="uk-container">
            <div uk-grid>
                <div class="uk-width-1-3@m flex-center">
                    <h2>Anmeldung</h2>
                </div>
                <div class="uk-width-2-3@m">
                    <form class="uk-form-stacked">
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Vorname *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Nachname *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-4-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Straße *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Hausnummer *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">PLZ *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-4-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Ort *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">E-mail*</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Telefon</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <div class="uk-form-label">Lieblingstermin für die Mühlenbäcker *</div>
                                    <div class="uk-form-controls">
                                        <label><input class="uk-radio" type="radio" name="radio1" required> Freitag, 25.
                                            Oktober 2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio1" required> Samstag, 26.
                                            Oktober 2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio1" required> Freitag, 1.
                                            November 2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio1" required> Samstag, 2.
                                            November 2019 </label>
                                    </div>
                                </div>

                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <div class="uk-form-label">Ersatztermin für die Mühlenbäcker</div>
                                    <div class="uk-form-controls">
                                        <label><input class="uk-radio" type="radio" name="radio12"> Freitag, 25. Oktober
                                            2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio12"> Samstag, 26. Oktober
                                            2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio12"> Freitag, 1. November
                                            2019 </label><br>
                                        <label><input class="uk-radio" type="radio" name="radio12"> Samstag, 2. November
                                            2019 </label>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <p>*Pflichtfelder</p>
                        <div class="uk-margin">
                            <button class="uk-button uk-button-default btn-anm" type="reset">Zürücksetzen</button>
                            <button class="uk-button uk-button-default btn-anm">Anmelden</button>

                        </div>

                    </form>
                </div>


            </div>
        </div>
    </section>
    <section id="block4">
        <div class="uk-position-relative uk-visible-toggle " tabindex="-1" uk-slider>

            <ul class="uk-slider-items uk-grid">
                <li class="uk-width-4-5">
                    <div class="uk-grid-collapse" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-panel">
                                <img src="img/kaese.jpg" alt="">
                            </div>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="bg-rezept-text">
                                <h2 class="black">Käsegebäck</h2>
                                <ul uk-accordion="multiple: true">
                                    <li>
                                        <a class="uk-accordion-title" href="#">Rezept einblenden</a>
                                        <a class="uk-accordion-title ausblenden" href="#">Rezept ausblenden</a>
                                        <div class="uk-accordion-content">
                                            <div class="rezept">
                                                <b>Zutaten:</b><br>
                                                200 g Emmentaler (gerieben)<br>
                                                200 g Butter<br>
                                                250 g Weizenvollkornmehl<br>
                                                ½ TL Salz<br>
                                                2 Eigelb<br>
                                                2 TL Milch<br>
                                                Kümmel, Paprika<br><br>
                                                Zubereitung:<br><br>
                                                Mehl, Käse und kalte Butterstücke schnell verkneten und eine halbe
                                                Stunde kühl stellen. Danach den Teig nach und nach auf einer bemehlten
                                                Platte ausrollen und beliebige Formen ausstechen. Eigelb und Milch
                                                verrühren, die Teile damit bestreichen, mit Kümmel oder Paprika
                                                bestreuen. Bei 180 Grad auf mittlerer Schiene eine gute Viertelstunde
                                                backen und auf dem Blech auskühlen lassen.

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </li>
                <li class="uk-width-4-5">
                    <div class="uk-grid-collapse" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-panel">
                                <img src="img/brot.jpg" alt="">
                            </div>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="bg-rezept-text">
                                <h2 class="black">Weizen-Dinkel-Semmeln</h2>
                                <ul uk-accordion="multiple: true">
                                    <li>
                                        <a class="uk-accordion-title" href="#">Rezept einblenden</a>
                                        <a class="uk-accordion-title ausblenden" href="#">Rezept ausblenden</a>
                                        <div class="uk-accordion-content">
                                            <div class="rezept">
                                                <b>Zutaten:</b><br>
                                                250 g Weizenvollkornmehl<br>
                                                250 g Dinkelvollkornmehl<br>
                                                5 g Zucker<br>
                                                5 g Butter<br>
                                                10 g Salz<br>
                                                1 Päckchen Trockenhefe<br>
                                                300 ml warmes Wasser<br><br>
                                                Zubereitung:<br><br>
                                                Alle Zutaten bis auf die Hefe mischen. Die Hefe im warmen Wasser
                                                auflösen und dazugeben. Den Teig zwei bis drei Minuten gut durchkneten.
                                                Zu einem Ballen formen und 20 Minuten bei 25 Grad in einer zugedeckten
                                                Schüssel aufgehen lassen. Danach zwölf Teile des Teigs zu Semmeln
                                                rollen. Auf ein Backblech setzen, abdecken, weitere 30 Minuten ruhen
                                                lassen. Mit einem scharfen Küchenmesser jede Teigrolle einkerben. Den
                                                Backofen auf 230 Grad vorheizen. In die Fettpfanne des Backofens gut 200
                                                ml heißes Wasser gießen, das Blech mit den Semmeln in den Ofen schieben
                                                und 25 Minuten backen. (Vor dem Backen kann man die Teigrollen auch mit
                                                Sonnenblumen- oder Kürbiskernen, mit Kümmel oder Mohn bestreuen.)

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </li>
                <li class="uk-width-4-5">
                    <div class="uk-grid-collapse" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-panel">
                                <img src="img/sesam.jpg" alt="">
                            </div>
                        </div>
                        <div class="uk-width-1-2@m">
                            <div class="bg-rezept-text">
                                <h2 class="black">Sesamkugeln<</h2>
                                <ul uk-accordion="multiple: true">
                                    <li>
                                        <a class="uk-accordion-title" href="#">Rezept einblenden</a>
                                        <a class="uk-accordion-title ausblenden" href="#">Rezept ausblenden</a>
                                        <div class="uk-accordion-content">
                                            <div class="rezept">
                                                <b>Zutaten:</b><br>
                                                180 g Weizenvollkornmehl<br>
                                                180 g Butter<br>
                                                120 g Sesam<br>
                                                120 g Honig<br>
                                                Zimt, Vanille, Salz, Nelken, Mandeln<br><br>
                                                Zubereitung:<br><br>
                                                Sesam rösten, mit Butter und Honig verrühren, die Gewürze und das Mehl
                                                hinzufügen. Den Teig etwa eine Stunde kühl stellen. Danach walnussgroße
                                                Kugeln formen, in die Mitte eine Mandel drücken und auf dem Backblech
                                                etwa zehn Minuten bei 210 Grad auf mittlerer Schiene backen.

                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </li>

            </ul>

            <a class="uk-position-center-left uk-position-small " href="#" uk-slidenav-previous
               uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small " href="#" uk-slidenav-next
               uk-slider-item="next"></a>

        </div>
    </section>
    <section id="block5" class="bg-form">
        <div class="uk-container">
            <h2 class="white">Unser besonderes Mehrsortiment für Sie</h2>
            <h5 class="form-h5">Ich möchte das Spezial-Angebot des Mehlsortiments in Anspruch nehmen</h5>

            <form class="uk-form-stacked">
                <div class="" uk-grid>
                    <div class="uk-width-1-2@m">
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Vorname *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Nachname *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-4-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Straße *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Hausnummer *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">PLZ *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-4-5@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Ort *</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">E-mail*</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div class="uk-margin">
                                    <label class="uk-form-label" for="form-stacked-text">Telefon</label>
                                    <div class="uk-form-controls">
                                        <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                               required>
                                    </div>
                                </div>
                            </div>



                    </div>
                        <ul uk-accordion="multiple: true">
                            <li>
                                <a class="uk-accordion-title" href="#">Abweichende adresse anzeigen</a>
                                <a class="uk-accordion-title ausblenden" href="#">Abweichende adresse ausblenden</a>
                                <div class="uk-accordion-content">
                                    <div class="uk-grid-small" uk-grid>
                                        <div class="uk-width-1-2@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">Vorname *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-2@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">Nachname *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="uk-grid-small" uk-grid>
                                        <div class="uk-width-4-5@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">Straße *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">Hausnummer *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-1-5@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">PLZ *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-width-4-5@s">
                                            <div class="uk-margin">
                                                <label class="uk-form-label" for="form-stacked-text">Ort *</label>
                                                <div class="uk-form-controls">
                                                    <input class="uk-input" id="form-stacked-text" type="text" placeholder=""
                                                           required>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                            </li>
                        </ul>
                        <p>*Pflichtfelder</p>
                    </div>
                    <div class="uk-width-1-2@m">

                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-checkbox" type="checkbox">&nbsp;&nbsp;Mehlsortiment Back ma's
                                     <small>Dinkeldunst, Weizenvollkorn, Rotten Type 815</small>
                            </label><br>
                        </div>
                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-checkbox" type="checkbox">&nbsp;&nbsp;Mehlsortiment Italiano
                                <small>Tipo 00, Weizen Type 2000, Hanfmehl</small>
                            </label><br>
                        </div>
                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-checkbox" type="checkbox">&nbsp;&nbsp;Mehlsortiment Christ Kindl
                                <small>Weizen Type 550, Roggen Type 610, Hanfmehl</small>
                            </label><br>
                        </div>
                        <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                            <label><input class="uk-checkbox" type="checkbox">&nbsp;&nbsp;Ich will mich erst bei der Mühlenbäckerei entscheiden, ob ich das Spezial-Abo in Anspruch nehme.
                            </label><br>
                        </div>
                    </div>

                </div>
                <div class="uk-margin">
                    <button class="uk-button uk-button-default btn-anm" type="reset">Zürücksetzen</button>
                    <button class="uk-button uk-button-default btn-anm">Anmelden</button>

                </div>
            </form>
        </div>

    </section>
    <footer>
        <div class="uk-container">
            <!-- This is a button toggling the modal with the default close button -->
            <button class="uk-button uk-button-default btn-footer uk-margin-small-right" type="button" uk-toggle="target: #modal-close-default">Impressum</button>
            <button class="uk-button uk-button-default btn-footer uk-margin-small-right" type="button" uk-toggle="target: #modal-close-default2">Datenschutz</button>

            <!-- This is the modal with the default close button -->
            <div id="modal-close-default" uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <h2 class="uk-modal-title">Impressum</h2>
                    <p>Getreidemühle Korn und Schrot e.K.<br>
                        Susanne Miller<br>
                        Schanzlweg 37<br>
                        94315 Straubing<br>
                        Tel: 09421/12340<br>
                        Fax: 09421/12344<br>
                        www.kornundschrot.de<br>
                        kontakt@kornundschrot.de<br>
                        <br><br>
                        Amtsgericht Straubing/Registersachen, XXX020101<br>
                        Umsatzsteuer ID: DE787654

                    </p>
                </div>
            </div>

            <!-- This is the modal with the default close button -->
            <div id="modal-close-default2" uk-modal>
                <div class="uk-modal-dialog uk-modal-body">
                    <button class="uk-modal-close-default" type="button" uk-close></button>
                    <h2 class="uk-modal-title">Datenschutz</h2>
                    <p>Datenschutzhinweise gemäß EU-Datenschutz-Grundverordnung (DSGVO)<br>
                        Wir setzen die seit dem 25. Mai 2018 geltende EU-Datenschutz-Grundverordnung (DSGVO) um, insbesondere, wenn es um die Verarbeitung Ihrer personenbezogenen Daten durch uns sowie um die Ihnen dabei zustehenden Rechte geht. Die Hinweise werden soweit erforderlich aktualisiert und sind im Detail nachlesbar unter: http://www.kornundschrot.de/datenschutz.php.
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
</body>
</html>