<!DOCTYPE html>
<html>
<head>
    <title>Aufgabe.info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/uikit.min.css"/>
    <link rel="stylesheet" href="css/custom.css"/>
    <script src="js/custom.js"></script>
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/6859/tween.min_1.js"></script>

</head>
<body>

<div uk-sticky="media: 960"
     class="uk-navbar-container tm-navbar-container uk-sticky uk-sticky-fixed uk-active uk-sticky-below">
    <div class="uk-container">
        <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
            <div class="uk-navbar-left">
                <ul class="uk-iconnav ">

                    <li><a href="/" uk-icon="icon: home"></a></li>

                </ul>
            </div>
            <div class="uk-navbar-right">
                <a href="." class="refresh" uk-icon="icon: refresh"></a>
            </div>
        </nav>


    </div>
</div>
<div class="uk-container">
    <h1>Impressum</h1>

    <h2>Angaben gem&auml;&szlig; &sect; 5 TMG</h2>
    <p>Vladislav Mazur<br />
        Viktro-von-Scheffel-str-33<br />
        91327 Goessweinstein</p>

    <h2>Kontakt</h2>
    <p>Telefon: 015238796345<br />
        E-Mail: vladislav@tonkom.de</p>
</div>
</body>
</html>